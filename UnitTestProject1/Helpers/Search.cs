﻿using System;
using System.Collections;
using NUnit.Framework;
using System.IO;
using Newtonsoft.Json;
using UnitTestProject1.POCO;
using Newtonsoft.Json.Linq;
using UnitTestProject1.Util;
using System.Collections.Generic;

namespace UnitTestProject1.Helpers
{
    class Search : TestHelperBase
    {
        [Test, TestCaseSource("Query")]
        public void SearchTest(Query query)
        {
            manager.Login.logIn(DataManager.Credentials);
            manager.Nav.openSearchPage();

            manager.Lookup.searchForAuto(query.query);
        }

        public static IEnumerable<Query> Query()
        {
            using (StreamReader r = new StreamReader("C:\\SpaceOddity\\WorkHard\\" +
                "stud\\fourthCourse\\testing(2)\\UnitTestProject1\\" +
                "UnitTestProject1\\data\\query.json"))
            {
                String json = r.ReadToEnd();
                var des = (Queries)JsonConvert.DeserializeObject(json, typeof(Queries));
                return des.queries;
            }
        }
    }
}
