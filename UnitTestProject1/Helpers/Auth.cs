﻿using NUnit.Framework;
using UnitTestProject1.Util;
using UnitTestProject1.POCO;

namespace UnitTestProject1.Helpers
{
    class Auth : TestHelperBase
    {
        [Test]
        public void OpenMainTest()
        {
            manager.Nav.openMainPage();
        }

        [Test]
        public void OpenSearchTest()
        {
            manager.Nav.openSearchPage();
        }

        [Test]
        public void LoginTest()
        {
            manager.Login.logIn(DataManager.Credentials);
        }
    }
}
