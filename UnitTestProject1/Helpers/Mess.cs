﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using UnitTestProject1.Util;
using UnitTestProject1.POCO;

namespace UnitTestProject1.Helpers
{
    class Mess : TestHelperBase
    {
        [Test]
        public void WriteMessageTest()
        {
            manager.Login.logIn(DataManager.Credentials);
            manager.Nav.openMainPage();

            manager.Message.composeMessage();
        }

        [Test]
        public void OpenMyMessagesTest()
        {
            manager.Login.logIn(DataManager.Credentials);

            manager.Message.openMyMessages();
        }
    }
}
