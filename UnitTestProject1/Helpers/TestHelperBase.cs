﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using UnitTestProject1.Util;

namespace UnitTestProject1.Helpers
{
    [TestFixture]
    class TestHelperBase
    {
        protected TestManager manager;

        [SetUp]
        public void SetupTest()
        {
            manager = TestManager.Instance;
            manager.Nav.openMainPage();
        }

        protected bool IsElementPresent(By by)
        {
            try
            {
                manager.Driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        protected bool IsAlertPresent()
        {
            try
            {
                manager.Driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        protected void WaitForPageLoad(String name)
        {
            WebDriverWait _wait = new WebDriverWait(manager.Driver, new TimeSpan(0, 1, 0));
            _wait.Until(d => d.FindElement(By.Name(name)));
        }
    }

}
