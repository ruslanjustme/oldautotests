﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

using UnitTestProject1.Helpers;
using UnitTestProject1.Workers;

namespace UnitTestProject1.Util
{
    class TestManager
    {
        private IWebDriver driver;
        private string baseURL;
        private bool isIn = false;

        private static volatile TestManager instance;
        private static readonly object syncLock = new object();

        private TestManager()
        {
            DataManager.StartUp();
            baseURL = DataManager.BaseURL;

            driver = new ChromeDriver();

            Nav = new Nav(this);
            Login = new Login(this);
            Message = new Message(this);
            Lookup = new Lookup(this);
        }

        ~TestManager()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
        }

        public static TestManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncLock)
                    {

                        instance = new TestManager();
                    }
                }
                return instance;
            }
        }

        public IWebDriver Driver
        {
            get
            {
                return driver;
            }
        }
        public string BaseURL
        {
            get
            {
                return baseURL;
            }
        }

        public Nav Nav
        {
            get;
        }

        public Login Login
        {
            get;
        }

        public Message Message
        {
            get;
        }

        public Lookup Lookup
        {
            get;
        }
    }

}
