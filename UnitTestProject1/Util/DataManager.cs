﻿using System;
using System.IO;
using Newtonsoft.Json;
using UnitTestProject1.POCO;
using Newtonsoft.Json.Linq;

namespace UnitTestProject1.Util
{
    class DataManager
    {
        private static Credentials credentials;
        private static String query;
        private static String baseURL;
        private static JToken token;

        public static void StartUp()
        {
            using (StreamReader r = new StreamReader("C:\\SpaceOddity\\WorkHard\\" +
                "stud\\fourthCourse\\testing(2)\\UnitTestProject1\\" +
                "UnitTestProject1\\data\\projectSettings.json"))
            {
                string json = r.ReadToEnd();
                token = JObject.Parse(json);
            }
        }

        public static String Query
        {
            get
            {
                if (query == null)
                {
                    query = (String)token.SelectToken("Query");
                }
                return query;
            }
        }

        public static String BaseURL
        {
            get
            {
                if (baseURL == null)
                {
                    baseURL = (String)token.SelectToken("BaseURL");
                }
                return baseURL;
            }
        }

        public static Credentials Credentials
        {
            get
            {
                if (credentials == null)
                {
                    JToken user = token.SelectToken("Credentials");
                    credentials = user.ToObject<Credentials>();
                }
                return credentials;
            }
        }
    }

}
