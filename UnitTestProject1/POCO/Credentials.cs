﻿using System;

namespace UnitTestProject1.POCO
{
    public class Credentials
    {
        public String Login
        {
            get;
            set;
        }

        public String Password
        {
            get;
            set;
        }
    }
}
