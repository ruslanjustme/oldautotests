﻿using System;
using OpenQA.Selenium;
using UnitTestProject1.Util;
using UnitTestProject1.POCO;

namespace UnitTestProject1.Workers
{
    class Lookup : Worker
    {
        public Lookup(TestManager manager) : base(manager)
        {
        }

        public void searchForAuto(String query)
        {
            manager.Driver.FindElement(By.CssSelector("input[name=\"keywords\"]")).Clear();
            manager.Driver.FindElement(By.CssSelector("input[name=\"keywords\"]")).SendKeys(query);
            manager.Driver.FindElement(By.Name("submit")).Click();
        }
    }
}
