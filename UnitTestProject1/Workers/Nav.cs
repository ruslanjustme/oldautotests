﻿using System;
using OpenQA.Selenium;
using UnitTestProject1.Util;
using UnitTestProject1.POCO;
using UnitTestProject1.Workers;

namespace UnitTestProject1.Helpers
{
    class Nav: Worker
    {
        public Nav(TestManager manager) : base(manager)
        {
        }

        public void openMainPage()
        {
            manager.Driver.Navigate().GoToUrl(manager.BaseURL);
        }

        public void openSearchPage()
        {
            manager.Driver.Navigate().GoToUrl(manager.BaseURL + "/search.php");
        }
    }
}
