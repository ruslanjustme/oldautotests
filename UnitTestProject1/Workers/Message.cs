﻿using System;
using OpenQA.Selenium;
using UnitTestProject1.Util;
using UnitTestProject1.POCO;

namespace UnitTestProject1.Workers
{
    class Message : Worker
    {
        public Message(TestManager manager) : base(manager)
        {
        }

        public void openMyMessages()
        {
            manager.Driver.Navigate().GoToUrl(manager.BaseURL + "/search.php?search_id=unanswered");
            manager.Driver.FindElement(By.LinkText("Ваши сообщения")).Click();
            manager.Driver.FindElement(By.LinkText("Помогите выбрать б\\у автомобиль.")).Click();
        }

        public void composeMessage()
        {
            manager.Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(20));

            manager.Driver.FindElement(By.LinkText("Активные темы")).Click();

            manager.Driver.FindElement(By.LinkText("Удаление вмятин без покраски")).Click();
            manager.Driver.FindElement(By.XPath("(//img[@alt='Ответить на тему'])[2]")).Click();
            manager.Driver.FindElement(By.Name("message")).Click();
            manager.Driver.FindElement(By.Name("message")).Clear();
            manager.Driver.FindElement(By.Name("message")).SendKeys("testing just for meooow");
            manager.Driver.FindElement(By.Name("post")).Click();
        }

    }
}
