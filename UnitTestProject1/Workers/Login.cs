﻿using System;
using OpenQA.Selenium;
using UnitTestProject1.Util;
using UnitTestProject1.POCO;
using UnitTestProject1.Workers;

namespace UnitTestProject1.Workers
{
    class Login: Worker
    {
        private bool isIn = false;

        public Login(TestManager manager) : base(manager)
        {
        }

        public void logIn(Credentials acc)
        {
            manager.Nav.openMainPage();

            if (!isIn)
            {
                manager.Driver.Navigate().GoToUrl(manager.BaseURL + "/ucp.php?mode=login");
                manager.Driver.FindElement(By.LinkText("Вход")).Click();

                manager.Driver.FindElement(By.Name("username")).Clear();
                manager.Driver.FindElement(By.Name("username")).SendKeys(acc.Login);
                manager.Driver.FindElement(By.Name("password")).Clear();
                manager.Driver.FindElement(By.Name("password")).SendKeys(acc.Password);
                manager.Driver.FindElement(By.Name("login")).Click();

                isIn = true;
            }
        }
    }
}
